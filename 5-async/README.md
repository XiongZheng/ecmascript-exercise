运行json-server

```
yarn json-server
```

访问 `http://localhost:3000/api` 可以返回结果
```json
{ 
  "name": "hello" 
}
```

练习结果：打开index.html，在页面显示hello
