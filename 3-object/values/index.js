export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  return Object.values(source).reduce((current, next) => {
    return parseInt(current, 10) + parseInt(next, 10);
  });
}
